<?php
/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('funzioni.php');

lugheader ('Linux Day ' . conf('current_year'),
    ['https://unpkg.com/leaflet@1.5.1/dist/leaflet.css', makeurl('/registra/registra.css')],
    ['https://unpkg.com/leaflet@1.5.1/dist/leaflet.js']
);

$year = conf('current_year');

if (file_exists($events_file)) {
    $events = json_decode(file_get_contents($events_file));

    $events = array_filter($events, function($a) {
        return $a->approvato;
    });

    usort($events, function($a, $b) {
        if ($a->prov < $b->prov)
            return -1;
        else if ($b->prov < $a->prov)
            return 1;
        else
            return 0;
    });
}
else {
    $events = [];
}

?>

<?php if ($year == 2021 || $year == 2022): ?>
    <?php

    $running_c4p = $year == date('Y') && true;

    $sessions = conf('sessions');
    $active_lives = false;

    foreach($sessions as $session_slug => $session_meta) {
        if ($session_meta->live) {
            $active_lives = true;
            break;
        }
    }

    ?>

    <?php if ($active_lives): ?>
        <div class="row mb-5">
            <div class="col-12 text-center mb-3">
                <p class="lead">SIAMO ONLINE</p>
            </div>

            <?php foreach($sessions as $session_slug => $session_meta): ?>
                <?php if ($session_meta->live): ?>
                    <div class="col-6">
                        <div class="alert alert-info text-center">
                            <a href="<?php echo makeurl('programma/live.php?slug=' . $session_slug) ?>">
                                Clicca qui per la sessione live:<br><strong><?php echo $session_meta->label ?></strong>
                            </a>
                        </div>
                    </div>
                <?php endif ?>
            <?php endforeach ?>

            <div class="col-12">
                <div class="alert alert-info text-center">
                    <a href="<?php echo makeurl('programma') ?>">
                        Clicca qui per il programma completo.
                    </a>
                </div>
            </div>
        </div>
    <?php endif ?>

    <?php if($running_c4p && date('Y-m-d') < conf('talks_date')): ?>
        <div class="alert alert-info call4papers row mb-5 ml-5 mr-5 p-5">
            <div class="col-sm-2">
                <img src="<?php echo makeurl('/immagini/megafono.svg') ?>">
            </div>
            <div class="col-sm-10">
                Partecipa alla <a href="<?php echo makeurl('/partecipa') ?>">Call for Papers</a> e candida il tuo talk per il Linux Day Online!
            </div>
        </div>
    <?php endif ?>

    <div class="highlight-box mb-5">
        <strong><?php echo conf('human_date') ?></strong> torna la principale manifestazione italiana dedicata a Linux, al software libero, alla cultura aperta ed alla condivisione.
    </div>

    <?php if ($year == 2021): ?>
        <p class="lead">
            Dopo l'esperienza totalmente online del 2020, l'edizione 2021 del Linux Day si svolgerà sia dal vivo in alcune città che online: tanti talk in diretta, tante sessioni parallele, e la possibilità di entrare in contatto con appassionati, curiosi, professionisti e volontari in ogni parte d'Italia!
        </p>
    <?php else: ?>
        <p class="lead">
            Il Linux Day torna in presenza in tante città, e con l'edizione online per tutti gli altri: tanti talk in diretta, tante sessioni parallele, e la possibilità di entrare in contatto con appassionati, curiosi, professionisti e volontari in ogni parte d'Italia!
        </p>
    <?php endif ?>

    <div class="row mt-5 mb-5 align-items-center">
        <div class="col">
            <img class="img-fluid" src="<?php echo makeurl('/promo/?format=300x250') ?>">

            <?php if ($year == 2021): ?>
                <p class="mt-2">
                    <small><i>Il banner 2021 è opera<br>di Giuseppe "peppenna" Penna e del <a href="https://lugce.it/" rel="nofollow">LUGCE</a></i></small>
                </p>
            <?php endif ?>
        </div>
        <div class="col">
            <?php if ($year == 2021): ?>
                <p>
                    Dati, dati, dati... Ma dati a chi?
                </p>
                <p>
                    Il Linux Day 2021 è dedicato alle informazioni che alimentano il software, ed alle loro infinite fonti, rappresentazioni, trasformazioni e utilizzi: dati personali, dati pubblici, dati privati, dati rubati, dati liberi, big data, linked data, open data e dataset.
                </p>
                <p>
                    Per raccontare le esperienze virtuose, mettere in guardia sugli utilizzi meno virtuosi, esplorare strumenti e piattaforme che ne permettono la gestione, la tutela e la condivisione.
                </p>
            <?php elseif ($year == 2022): ?>
                <p>
                    Il tema del Linux Day 2022 è... undefined.
                </p>
                <p>
                    Davvero, non è uno scherzo: dopo due anni di pandemia e tante difficoltà, l'edizione 2022 è una tela bianca su cui poter scrivere ciò che si preferisce, ciò che più appassiona o più interessa, ciò che maggiormente coinvolge la propria community.
                </p>
            <?php endif ?>
        </div>
    </div>

    <hr>

    <p>
        Seguici su <a href="https://twitter.com/LinuxDayItalia">Twitter</a> (<a href="https://twitter.com/search?q=LinuxDay2023&f=live">#LinuxDay2023</a>) o <a href="https://www.facebook.com/LinuxDayItalia">Facebook</a>, o <a href="http://www.ils.org/newsletter">iscriviti alla newsletter</a> per aggiornamenti.
    </p>

    <p>
        L'accesso al Linux Day, sia fisico che virtuale, è libero e gratuito!
    </p>

<?php else: ?>
    <div class="highlight-box mb-5">
        <strong><?php echo conf('human_date') ?></strong> torna la principale manifestazione italiana dedicata a Linux, al software libero, alla cultura aperta ed alla condivisione: decine di eventi in tutta Italia, centinaia di volontari coinvolti e migliaia di visitatori per celebrare insieme la libertà digitale!
    </div>
    <p>
        Dal 2001 il Linux Day è una iniziativa distribuita per conoscere ed approfondire Linux ed il software libero. Si compone di numerosi eventi locali, organizzati autonomamente da gruppi di appassionati nelle rispettive città, tutti nello stesso giorno. In tale contesto puoi trovare talks, workshops, spazi per l'assistenza tecnica, gadgets, dibattiti e dimostrazioni pratiche.
    </p>
    <p>
        Seguici su <a href="https://twitter.com/LinuxDayItalia">Twitter</a> (<a href="https://twitter.com/search?q=LinuxDay<?php echo $year ?>&f=live">#LinuxDay<?php echo $year ?></a>) o <a href="https://www.facebook.com/LinuxDayItalia">Facebook</a>, o <a href="http://www.ils.org/newsletter">iscriviti alla newsletter</a> per aggiornamenti.
    </p>
    <p>
        L'accesso al Linux Day è libero e gratuito!
    </p>

    <hr>

    <?php if (hasBanner()): ?>
        <p class="text-center">
            <img class="banner" src="<?php echo makeurl('/promo/?format=300x250') ?>" alt="Linux Day <?php echo $year ?>">
        </p>
    <?php endif ?>

    <?php foreach(conf('theme') as $theme_line): ?>
        <p>
           <?php echo $theme_line ?>
        </p>
    <?php endforeach ?>
<?php endif ?>


<?php if(conf('is_physical')): ?>
    <?php if(count($events) != 0): ?>
        <hr />
        <div id="local" class="mb-2 alert alert-info">
            Qui sotto trovi la mappa dei Linux Day locali che si svolgeranno <?php echo conf('human_date') ?>. Per partecipare all'edizione online del Linux Day, consulta <a href="<?php echo makeurl('/programma') ?>">questa pagina</a>.
        </div>

        <div id="mapid"></div>

        <script>
        $(document).ready(function() {
            var mymap = L.map('mapid').setView([42.204, 11.711], 6);

            L.tileLayer('https://c.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
                maxZoom: 18,
            }).addTo(mymap);

            <?php foreach($events as $event): ?>
                <?php if(!empty($event->coords)): ?>
                    L.marker([<?php echo $event->coords ?>]).bindPopup('<b><?php echo str_replace("'", "\\'", $event->group) ?></b> </br> <?php echo str_replace("'", "\\'", $event->city) ?> (<?php echo($event->prov) ?>) </br> <a href="<?php echo($event->web)?>"><?php echo($event->web)?></a>').addTo(mymap);
                <?php endif ?>
            <?php endforeach ?>
        });
        </script>

        <table class="table">
            <thead>
                <tr>
                    <th width="50%">Organizzatore</th>
                    <th width="45%">Città</th>
                    <th width="5%">Link</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($events as $event): ?>
                    <tr>
                    <td><?php echo $event->group ?></td>
                    <td><?php echo $event->city ?> (<?php echo $event->prov ?>)</td>
                    <td><a href="<?php echo $event->web ?>">Link</a></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    <?php endif ?>
<?php endif ?>

<?php
lugfooter ();
