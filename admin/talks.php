<?php
/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');
fullpageheader('Linux Day ' . conf('current_year') . ': Admin');

if (empty($_SESSION['admin']) || $_SESSION['admin'] != 'S') {
	?>

	<div class="alert alert-danger">
		Questa pagina è riservata agli amministratori.
	</div>

	<?php
}
else {
	?>
	<h1 class="h1 title">Talks Registrati</h1>
	<table class="table">
		<thead>
			<tr>
				<th scope="col">Relatore</th>
				<th scope="col">Titolo</th>
				<th scope="col">Sessione</th>
				<th scope="col">Abstract</th>
                <th scope="col">Approvato</th>
				<th scope="col">Note</th>
			</tr>
		</thead>
		<tbody>

		<?php

		$talks_file = '../data/talks' . conf('current_year') . '.json';
		$talks = json_decode(file_get_contents($talks_file));
		foreach($talks as $t) {
			?>

			<tr>
				<td>
					<?php echo($t->name) ?><br>
					<?php echo($t->email) ?><br>
					<?php echo($t->ip) ?>
				</td>
				<td><?php echo($t->talk) ?></td>
				<td><?php echo($t->session) ?></td>
				<td><?php echo(nl2br($t->abstract)) ?></td>
				<td><?php echo($e->approved ? 'SI' : 'NO') ?></td>
				<td><?php echo(nl2br($t->notes)) ?></td>
			</tr>

			<?php
		}

		?>

		</tbody>
	</table>

<?php
}

lugfooter ();
