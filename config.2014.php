<?php

date_default_timezone_set("Europe/Rome");
$current_year = '2014';
$computer_date = '2014-10-25';
$shipping_date = '2014-10-25';
$human_date = 'Sabato 25 Ottobre 2014';
$administrators = ['bob@linux.it'];

$is_virtual = false;
$is_physical = true;
$sessions = [];
$talks_date = null;

$sponsors = [
    'Linux Professional Institute Italia' => (object) [
        'logo' => '/immagini/lpi.png',
        'link' => 'https://www.lpi.org/it/',
    ],
	'Carrara Computing International' => (object) [
        'logo' => '/immagini/cci.png',
    	'link' => 'http://www.ccinetwork.it/',
    ],
];

$supporters = [
	'Wikimedia Italia' => (object) [
        'logo' => '/immagini/wikimedia.png',
        'link' => 'http://wikimedia.it/',
    ],
	'Creative Commons Italia' => (object) [
        'logo' => '/immagini/ccitalia.png',
        'link' => 'http://creativecommons.it/',
    ],
];

$patronages = [];

$theme = [];
