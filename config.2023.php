<?php

$internal_current_date_do_not_read = date('Y-m-d');

date_default_timezone_set("Europe/Rome");

$current_year = '2023';
$computer_date = '2023-10-28';
$shipping_date = '2022-10-13';
$human_date = 'Sabato 28 Ottobre 2023';
$administrators = ['bob@linux.it', 'gnu@linux.it', 'direttore@linux.it'];

$is_virtual  = false;
$is_physical = true;

$sessions = [];

$talks_date = '2022-09-30';
$human_talks_date = 'Venerdi 30 Settembre';

// alphabetical order
$sponsors = [];

$supporters = [
	'GARR' => (object) [
		'logo' => '/immagini/garr.png',
		'link' => 'https://garr.it/',
	],
];

$patronages = [];

$theme = [];

$ils_logo = 'immagini/ils-circle-200.png';
