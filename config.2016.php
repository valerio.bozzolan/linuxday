<?php

date_default_timezone_set("Europe/Rome");
$current_year = '2016';
$computer_date = '2016-10-22';
$shipping_date = '2016-10-22';
$human_date = 'Sabato 22 Ottobre 2016';
$administrators = ['bob@linux.it'];

$is_virtual = false;
$is_physical = true;
$sessions = [];
$talks_date = null;

$sponsors = [
    'Linux Professional Institute Italia' => (object) [
        'logo' => '/immagini/lpi.png',
        'link' => 'https://www.lpi.org/it/',
    ],
	'Carrara Computing International' => (object) [
        'logo' => '/immagini/cci.png',
    	'link' => 'http://www.ccinetwork.it/',
    ],
	'Yocto Project' => (object) [
        'logo' => '/immagini/yocto.png',
    	'link' => 'https://yoctoproject.org/',
    ],
	'Koan' => (object) [
        'logo' => '/immagini/koan.png',
    	'link' => 'http://koansoftware.com/',
    ],
];

$supporters = [];
$patronages = [];

$theme = [];
