<?php
/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once ('../funzioni.php');
lugheader ('Linux Day ' . conf('current_year') . ': Login');

$users_file = '../data/users.txt';
$message = '';

if (isset($_POST['action'])) {
	if ($_POST['action'] == 'login') {
		$input_email = isset($_POST['email']) ? $_POST['email'] : '';
		$input_password = isset($_POST['password']) ? $_POST['password'] : '';

		$file = file($users_file);

		foreach($file as $f) {
			$f = trim($f);
			list($email, $password, $active, $hash) = explode(';', $f);

			if ($active == 'S' && $email == $input_email) {
				if (password_verify($input_password, $password)) {
					$_SESSION['user_email'] = $email;
					$admins = conf('administrators');
					if (in_array($email, $admins)) {
						$_SESSION['admin'] = 'S';
					}
					$_SESSION['user_email'] = $email;
					$registration_link = makeurl('/registra/');
					$message = 'Autenticato! <a href="' . $registration_link . '">Clicca qui per procedere</a> alla registrazione o modifica del tuo evento.';
				}

				break;
			}
		}

		if (empty($message)) {
			sleep(3);
			$message = 'Credenziali non valide o account non ancora verificato.';
		}
	}
	else if ($_POST['action'] == 'registration') {
		$email = isset($_POST['email']) ? $_POST['email'] : '';
		$password = isset($_POST['password']) ? $_POST['password'] : '';
		$password_confirm = isset($_POST['password_confirm']) ? $_POST['password_confirm'] : '';
		$file = file($users_file);

		foreach($file as $f) {
			$f = trim($f);
			list($account_email, $account_password, $account_active, $account_hash) = explode(';', $f);
			if ($email == $account_email) {
				$presente = true;
			}
		}

		if (empty($email)) {
			$message = 'La Email non è stata definita';
		}
		if (empty($password)) {
			$message = 'La Password non è stata definita';
		}
		if ($password != $password_confirm) {
			$message = 'Le Password non corrispondono';
		}
		if (isset($presente) && $presente === true) {
			$message = "L'email è già registrata.";
		}

		if (empty($message)) {
			$random = random_string(20);
			$string = sprintf("%s;%s;N;%s\n", $email, password_hash($password, PASSWORD_BCRYPT), $random);
			file_put_contents($users_file, $string, FILE_APPEND);

			$text = "Visita questo URL per abilitare il tuo nuovo account Linux Day:\n" .
				makeurl('/user?action=validate&token=' . $random) .
				"\n\nUna volta verificato l'accesso, potrai registrare il tuo evento in questa pagina:\n" .
				makeurl('/registra') .
				"\n\nGrazie per la collaborazione!\nIl Team Linux Day\n";

			$headers = 'From: webmaster@linux.it' . "\r\n";
			mail($email, 'registrazione linux day', $text, $headers);

			$message = 'Ti è stata inviata una mail di conferma, clicca sul link per accedere al sito.';
		}
	}
	else if ($_POST['action'] == 'recover' && isset($_POST['email'])) {
		$params_email = $_POST['email'];
		if (empty($params_email)) {
			$message = 'La Email non è stata definita';
		}

		if (empty($message)) {
			$file = file($users_file);
			$new_file = [];
			foreach($file as $key => $f) {
				$f = trim($f);
				list($email, $password, $active, $hash) = explode(';', $f);
				if ($email == $params_email && $active == 'S') {
					$random = random_string(20);
					$new_file[] = sprintf("%s;%s;S;%s", $email, $password, $random);
					$message = 'Grazie. Abbiamo provveduto ad inviarti una email contenente il link per recuperare la tua password.';
					$text = "Caro utente, visita questo link per recuperare la tua password:\n" .
						makeurl('/user/index.php?action=newpassword&token=' . $random) .
						"\n\nRicordati che è valido fino ad un tuo utilizzo.\n\nGrazie per la collaborazione!\nIl Team Linux Day\n";
					$headers = 'From: webmaster@linux.it' . "\r\n";
					mail($email, 'recupero password Linux Day', $text, $headers);
				}
				else {
					$new_file[] = $f;
				}
			}

			if (empty($message)) {
				$message = "Impossibile procedere all'invio del token di recupero password: utente non trovato o non abilitato";
			}
			else {
				file_put_contents($users_file, join("\n", $new_file) . "\n");
			}
		}
	}
	else if ($_POST['action'] == 'newpassword') {
		$input_hash = isset($_POST['token']) ? $_POST['token'] : 'token_non_valido';
		$user_password = isset($_POST['password']) ? $_POST['password'] : 'B';
		$password_confirm = isset($_POST['password_confirm']) ? $_POST['password_confirm'] : 'A';

		if ($user_password === $password_confirm) {
			$file = file($users_file);
			$new_file = [];

			foreach($file as $key => $f) {
				$f = trim($f);
				list($email, $password, $active, $hash) = explode(';', $f);
				if ($hash === $input_hash) {
					$random = random_string(20);
					$new_file[] = sprintf("%s;%s;S;%s", $email, password_hash($user_password, PASSWORD_BCRYPT), $random);
					$message = 'Grazie, la nuova password è stata registrata';
				}
				else {
					$new_file[] = $f;
				}
			}
		}

		if (empty($message)) {
			$message = 'Controlla di aver scritto entrambe le password in modo corretto oppure il tuo token è errato.';
		}
		else {
			file_put_contents($users_file, join("\n", $new_file) . "\n");
		}
	}
}
else if (isset($_GET['action'])) {
	if ($_GET['action'] == 'validate') {
		$input_hash = isset($_GET['token']) ? $_GET['token'] : 'token_non_valido';
		$file = file($users_file);
		$new_file = [];

		foreach($file as $f) {
			$f = trim($f);
			list($email, $password, $active, $hash) = explode(';', $f);
			if ($hash == $input_hash) {
				$new_file[] = sprintf("%s;%s;S;%s", $email, $password, $hash);
				$message = 'Account verificato. Ora puoi autenticarti.';
			} else {
				$new_file[] = $f;
			}
		}

		if (empty($message)) {
			$message = 'Token non verificato.';
		}
		else {
			file_put_contents($users_file, join("\n", $new_file) . "\n");
		}
	}
	else if ($_GET['action'] == 'newpassword') {
		if (isset($_GET['token'])) {
		   	?>
			<h1 class="title">Scegli la nuova password</h1>
			<form method="POST" action="<?php echo makeurl('/user/index.php') ?>">
				<input type="hidden" name="action" value="newpassword">
				<input type="hidden" name="token" value="<?php echo $_GET['token'] ?>">

				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" class="form-control" id="password" name="password" required>
				</div>
				<div class="form-group">
					<label for="password_confirm">Conferma Password</label>
					<input type="password" class="form-control" id="password_confirm" name="password_confirm" required>
				</div>
				<button type="submit" class="btn btn-primary">Modifica la password</button>
			</form>

			<hr>
			<?php
		}
	}
	else if ($_GET['action'] == 'logout') {
		session_unset();
		$message = 'Hai effettuato il logout.';
	}
}

?>

<h2>Login / Registrazione Utente</h2>

<?php if(!empty($message)): ?>
	<div class="alert alert-info">
		<?php echo $message ?>
	</div>
<?php endif ?>

<?php if (conf('is_physical') == true): ?>
	<p>
		Autenticati per registrare il tuo Linux Day o modificare l'evento già caricato.
	</p>
	<p>
		<strong>Attenzione</strong>: questa funzione è dedicata a chi organizza un Linux Day nella propria città: per partecipare in veste di visitatore non c'é bisogno di registrarsi qui!
	</p>
<?php endif ?>

<form method="POST" action="<?php echo makeurl('/user/index.php') ?>">
	<input type="hidden" name="action" value="login">

	<div class="form-group">
		<label for="email">Indirizzo Email</label>
		<input type="email" class="form-control" id="email" name="email" required>
	</div>
	<div class="form-group">
		<label for="password">Password</label>
		<input type="password" class="form-control" id="password" name="password" required>
	</div>
	<button type="submit" class="btn btn-primary">Login</button>
</form>

<hr>

<?php

/*
	Per evitare la generazione di spam, il pannello di registrazione è attivo da
	un mese prima del Linux Day fino alla data effettiva
*/

$the_date = strtotime(conf('computer_date'));
$now = time();
$one_month_before = $the_date - (60 * 60 * 24 * 30);

if ($now < $limit && $now > $one_month_before): ?>
	<p>
		Hai già un account ma non ricordi la password? Scrivi qui il tuo indirizzo email.
	</p>

	<form method="POST" action="<?php echo makeurl('/user/index.php') ?>">
		<input type="hidden" name="action" value="recover">

		<div class="form-group">
			<label for="email">Indirizzo Email</label>
			<input type="email" class="form-control" id="email" name="email" required>
		</div>
		<button type="submit" class="btn btn-primary">Recupera la password</button>
	</form>

	<?php if (conf('is_physical') == true): ?>
		<hr>

		<p>
			Non hai ancora un account? Registrati qui! Riceverai una mail di conferma.
		</p>

		<form method="POST" action="<?php echo makeurl('/user/index.php') ?>">
			<input type="hidden" name="action" value="registration">

			<div class="form-group trap">
				<label for="name">Nome</label>
				<input type="text" class="form-control" id="name" name="name">
			</div>
			<div class="form-group">
				<label for="email">Indirizzo Email</label>
				<input type="email" class="form-control" id="email" name="email" required>
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input type="password" class="form-control" id="password" name="password" required>
			</div>
			<div class="form-group">
				<label for="password_confirm">Conferma Password</label>
				<input type="password" class="form-control" id="password_confirm" name="password_confirm" required>
			</div>
			<button type="submit" class="btn btn-primary">Registra</button>
		</form>
	<?php endif;
endif;

lugfooter ();
