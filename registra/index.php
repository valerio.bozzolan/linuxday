<?php
/*
Copyright (C) 2019  Italian Linux Society - http://www.linux.it

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
require_once ('../funzioni.php');
lugheader ('Linux Day ' . conf('current_year') . ': Registra Evento',
	['https://unpkg.com/leaflet@1.5.1/dist/leaflet.css', makeurl('/registra/registra.css')],
	['https://unpkg.com/leaflet@1.5.1/dist/leaflet.js', makeurl('/registra/registra.js')]
);

$is_admin = isset($_SESSION['admin']) && $_SESSION['admin'] == 'S';

$shipping_date = conf('shipping_date');
if ($shipping_date) {
	$past_shipping_date = date('Y-m-d') > conf('shipping_date');
}

$past_save_date = date('Y-m-d') > conf('computer_date');

if ($past_save_date) {
	?>
		<div class="alert alert-danger">
			È scaduto il termine per inserire il proprio LinuxDay. Ci vediamo l'anno prossimo!
		</div>

<?php
}
if (empty($_SESSION['user_email'])) {
	?>

	<div class="alert alert-danger">
		Questa pagina è riservata a chi ha un account registrato. <a href="<?php echo makeurl('/user') ?>">Vai su questa pagina</a> per autenticarti o registrarti.
	</div>

	<?php
}
else {
	$events_file = '../data/events' . conf('current_year') . '.json';

	$owner_email = $_SESSION['user_email'];
	if ($is_admin) {
		if (isset($_REQUEST['owner']))
			$owner_email = $_REQUEST['owner'];
	}

	$event = findEvent($events_file, $owner_email);
	$message = '';

	if (isset($_POST['action'])) {
		if ($_POST['action'] == 'save') {
			$illegal_shipping = $past_shipping_date && ($event->gadgets !== isset($_POST['gadgets']) && $event->gadgets_address !== $_POST['gadgets_address']);
			$not_prov = $_POST['prov'] == '-1';

			if ($illegal_shipping || $past_save_date || $not_prov) {
				if ($not_prov) {
					$message = "<b>ERRORE:</b> Devi indicare una provincia per poter presentare il tuo evento.";
				} else {
					$message = "<b>ERRORE:</b> Passata la data entro la quale potevi presentare il tuo evento o chiedere la spedizione dei materiali.";
				}
			}
			else {
				$event->owner = $owner_email;
				$event->group = $_POST['group'];
				$event->city = $_POST['city'];
				$event->prov = $_POST['prov'];
				$event->web = $_POST['web'];
				$event->coords = $_POST['coordinates'];
				$event->gadgets = isset($_POST['gadgets']);
				$event->gadgets_address = $_POST['gadgets_address'];

				if ($is_admin) {
					if ($event->approvato == false && isset($_POST['checkbox_approvato'])) {
						$event->approvato = true;
						$text = "Grazie! L'evento che hai organizzato per il LinuxDay è stato approvato ed ora è visibile sul sito!";
						$headers = 'From: webmaster@linux.it' . "\r\n";
						mail($event->owner, 'Approvazione evento LinuxDay', $text, $headers);
					}
					else if ($event->approvato == true && !isset($_POST['checkbox_approvato'])) {
						$event->approvato = false;
					}
				}

				$result = saveEvent($events_file, $event);
				$message = 'Evento salvato correttamente! Ricorda: deve essere approvato dagli amministratori prima che compaia nella mappa in homepage.';
			}
		}
	}

	if (isset($_GET['action'])) {
		if ($_GET['action'] == 'destroy') {
			$get_email = $_GET['owner'];
			$events = json_decode(file_get_contents($events_file));

			foreach($events as $index => $e) {
				if ($e->owner == $get_email) {
					$selected = $e;
					$selected_index = $index;
					break;
				}
			}

			if ($selected->owner == $_SESSION['user_email']  || $is_admin) {
				unset($events[$selected_index]);
				$events = array_values($events);
				file_put_contents($events_file, json_encode($events));

				?>
				<p class="alert alert-info">
					Eliminato con successo. <a href="<?php echo(makeurl('/registra'))?>">Clicca qui</a> per creare un nuovo evento o clicca salva senza ricaricare se hai cancellato l'evento per sbaglio.
				</p>
				<?php
			}
			else {
				?>
				<p class="alert alert-danger">
					Devi essere admin o essere il creatore dell'evento per poterlo eliminare.
				</p>
				<?php
			}
		}
	}

	?>

	<h2>Crea / Modifica Evento</h2>

	<p class="alert alert-info">
		In caso di problemi, scrivi a <a href="mailto:webmaster@linux.it">webmaster@linux.it</a>.
	</p>

	<p class="alert alert-info">
		Si raccomanda di creare, prima della registrazione, una pagina sul proprio sito da destinare al Linux Day dell'anno corrente, da eventualmente arricchire in seguito ma per almeno annunciare data e luogo della manifestazione. Le registrazioni non vengono approvate finché sul sito linkato non si trova nessuna informazione in merito al prossimo Linux Day.
	</p>
	<?php if(!empty($message)): ?>
		<div class="alert alert-info">
			<?php echo $message ?>
		</div>
	<?php endif ?>

	<form method="POST" action="<?php echo makeurl('/registra/index.php') ?>">
		<input type="hidden" name="action" value="save">

		<div class="form-group">
			<label for="group">Organizzatore</label>
			<input type="text" class="form-control" id="group" name="group" value="<?php echo $event->group ?>" required>
			<small class="form-text text-muted">Nome del gruppo organizzatore (LUG, associazione, scuola, ente...). Non il tuo nome e cognome!</small>
		</div>
		<div class="form-group">
			<label for="city">Città</label>
			<input type="text" class="form-control" id="city" name="city" value="<?php echo $event->city ?>" required>
		</div>
		<div class="form-group">
			<label for="city">Provincia</label>
			<?php prov_select('form-control', $event->prov) ?>
		</div>
		<div class="form-group">
			<label for="web">Sito Web</label>
			<input type="text" class="form-control" id="web" name="web" value="<?php echo $event->web ?>" required>
			<small class="form-text text-muted">URL della pagina su cui reperire informazioni sull'evento (e.g. http://miolug.it/linuxday). Non usate la homepage del vostro sito, piuttosto create una pagina temporanea da arricchire in seguito.</small>
		</div>
		<div class="form-group">
			<input type="hidden" name="coordinates" value="<?php echo $event->coords ?>">
			<label for="mapid">Mappa</label>
			<div id="mapid"></div>
			<small class="form-text text-muted">Posiziona il marker cliccando sulla mappa, per indicare il luogo dove si svolgerà il tuo evento. Sii il più preciso possibile!</small>
		</div>

		<?php if($shipping_date): ?>
			<div class="form-group form-check <?php echo($past_shipping_date ? 'hidden' : '') ?>">
				<input type="checkbox" class="form-check-input" id="gadgets" name="gadgets" <?php echo ($event->gadgets ? 'checked' : '') ?>>
				<label class="form-check-label" for="gadgets">Richiesta Materiali</label>
				<small class="form-text text-muted">Ogni anno Italian Linux Society invia agli organizzatori del Linux Day un pacco di spille, adesivi e materialie informativo. Spunta questa casella e compila il campo sotto per riceverlo anche tu!</small>
			</div>

			<div class="form-group <?php echo($past_shipping_date? 'hidden' : '') ?>">
				<label for="gadgets_address">Indirizzo Recapito</label>
				<textarea class="form-control" id="gadgets_address" name="gadgets_address" rows="7"><?php echo $event->gadgets_address ?></textarea>
			</div>

			<?php if($past_shipping_date):?>
				<div class="alert alert-danger">
					È scaduto il termine per richiedere l'invio dei gadget.
				</div>
			<?php endif ?>
		<?php endif ?>

		<?php if ($is_admin): ?>
			<input type="hidden" name="owner" value="<?php echo $owner_email ?>">

			<div class="form-group form-check">
				<input type="checkbox" class="form-check-input" id="checkbox_approvato" name="checkbox_approvato" <?php echo ($event->approvato ? 'checked' : '') ?>>
				<label class="form-check-label" for="gadgets">Evento approvato e da pubblicare</label>
			</div>
		<?php endif ?>

		<button type="submit" class="btn btn-primary" <?php echo($past_save_date ? 'disabled' : '') ?>>Salva</button>
	</form>
	<?php if ($event->existing !== false): ?>
		<a href="<?php echo(makeurl('/registra/index.php?action=destroy&owner=' . $owner_email)) ?>" class="btn btn-danger">Elimina evento</a>
	<?php endif ?>

	<?php
}

lugfooter ();
