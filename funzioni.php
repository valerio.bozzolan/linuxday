<?php

/*
LinuxDay
Copyright (C) 2019-2023  Italian Linux Society - http://www.linux.it and contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function conf($name, $default = null) {
    static $config_file = null;

    if ($config_file == null) {
        $path = $_SERVER['REQUEST_URI'];
        $actual_year = preg_replace('/^\/(20[0-9][0-9]).*$/', '\1', $path);
        $config_file = 'config.' . $actual_year . '.php';

        if (!file_exists($config_file)) {
            if (!file_exists('../' . $config_file)) {
                $configs = glob('config.*.php');
                $config = $configs[count($configs) - 1];
                list($useless, $year, $useless) = explode('.', $config);

                header('Location: /' . $year);
                exit();
            }
        }
    }

    require($config_file);
    if (!isset($$name)) {
        if($default) {
          return $default;
        } else {
          echo 'parametro ' . $name . ' non esistente';
        }
    }

    return $$name;
}

$events_file = __DIR__ . '/data/events' . conf('current_year') . '.json';
$days_file = __DIR__ . '/data/schedule' . conf('current_year') . '.json';

function slugify($text)
{
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

function makeurl($url) {
    if (substr($url, 0, 7) == 'http://' || substr($url, 0, 8) == 'https://') {
        return $url;
    }

    if (substr($url, 0, 1) != '/') {
        $url = '/' . $url;
    }

    return 'https://www.linuxday.it/' . conf('current_year') . $url;
    // return 'http://linuxday.local.it/' . conf('current_year') . $url;
}

function random_string($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $len = strlen($characters);
    $ret = '';

    for ($i = 0; $i < $length; $i++) {
        $ret .= $characters[rand(0, $len - 1)];
    }

    return $ret;
}

function findEvent($events_file, $current_user) {
    if (!empty($current_user) && file_exists($events_file)) {
        $events = json_decode(file_get_contents($events_file));
        foreach($events as $e) {
            if ($e->owner == $current_user) {
                $e->existing = true;
                return $e;
            }
        }
    }

    return (object) [
        'existing' => false,
        'owner' => $current_user,
        'group' => '',
        'city' => '',
        'prov' => null,
        'web' => '',
        'coords' => '42.204,11.711',
        'gadgets' => false,
        'gadgets_address' => '',
        'approvato' => false,
    ];
}

function saveEvent($events_file, $event) {
    if (file_exists($events_file)) {
        $events = json_decode(file_get_contents($events_file));
    }
    else {
        $events = [];
    }

    if (isset($event->existing) && $event->existing == false) {
        unset($event->existing);
        $events[] = $event;
    }
    else {
        foreach($events as $index => $e) {
            if ($event->owner == $e->owner) {
                $events[$index] = $event;
                break;
            }
        }
    }

    file_put_contents($events_file, json_encode($events));
}

function unrollHours()
{
    $hours = [];

    for($i = 10; $i < 19; $i++) {
        $hours[] = sprintf('%d:00', $i);
        $hours[] = sprintf('%d:30', $i);
    }

    return $hours;
}

function unrollDay($events, $only_day = -1, $review = false)
{
    $hours = unrollHours();
    $hours_index = 0;
    $accumulator = array_fill(0, count($events), false);
    $indexes = array_fill(0, count($events), 0);

    $rows = 0;
    for ($a = 0; $a < count($events); $a++) {
        $session_rows = 0;

        foreach($events[$a] as $e) {
            if ($e->slot == 60) {
                $session_rows += 2;
            }
            else {
                $session_rows += 1;
            }
        }

        if ($session_rows > $rows) {
            $rows = $session_rows + 1;
        }
    }

    foreach($hours as $h) {
        $hours_index++;
        if ($hours_index == $rows) {
            continue;
        }

        ?>
        <tr>
            <td><?php echo $h ?></td>

            <?php

            for($a = 0; $a < count($events); $a++) {
                if ($only_day != -1 && $only_day != $a) {
                    continue;
                }

                if ($accumulator[$a] == false) {
                    $event = $events[$a][$indexes[$a]];
                    $indexes[$a]++;
                    $accumulator[$a] = ($event->slot == 60);

                    if ($review == true) {
                        if (!isset($event->video)) {
                            $class = 'talk green';
                        }
                        else {
                            if ($event->video == 'todo') {
                                $class = 'talk red';
                            }
                            else {
                                $class = 'talk blue';
                            }
                        }
                    }
                    else {
                        $class = 'talk';
                    }

                    ?>

                    <td rowspan="<?php echo ($event->slot == 60 ? 2 : 1) ?>" class="<?php echo $class ?>">
                    <?php if($event->talk != 'empty'): ?>
                        <span class="title">
                            <?php if($review == false): ?>
                                <a href="<?php echo makeurl('programma/talk.php?slug=' . slugify($event->talk)) ?>"><?php echo $event->talk ?></a>
                            <?php else: ?>
                                <?php echo $event->talk ?>
                            <?php endif ?>
                        </span>
                        <span class="author"><?php echo $event->name ?></span>
                    <?php endif ?>
                    </td>

                    <?php
                }
                else {
                    $accumulator[$a] = false;
                }
            }

            ?>
        </tr>
        <?php
    }
}

function findTalk($schedule_file, $slug)
{
    if (file_exists($schedule_file)) {
        $talks = json_decode(file_get_contents($schedule_file));
        foreach($talks as $t) {
            foreach($t->events as $events_index => $events) {
                $hours_index = 0;

                foreach($events as $e) {
                    if (slugify($e->talk) == $slug) {
                        $hours = unrollHours();
                        $e->hour = $hours[$hours_index];
                        $e->day = $t->title;
                        $e->session = $t->columns[$events_index];
                        return $e;
                    }
                    else {
                        $hours_index += $e->slot == 30 ? 1 : 2;
                    }
                }
            }
        }
    }

    return null;
}

function findSession($slug)
{
    $sessions = conf('sessions');

    foreach($sessions as $s => $meta) {
        if ($s == $slug) {
            return $meta;
        }
    }

    return null;
}

function hasBanner()
{
    $year = conf('current_year');
    $format = '300x250';
    $file = sprintf('%s/promo/banner_%s_%s.png', __DIR__, $year, $format);
    return file_exists($file);
}

function saveTalk($talks_file, $talk) {
    if (file_exists($talks_file)) {
        $talks = json_decode(file_get_contents($talks_file));
    }
    else {
        $talks = [];
    }

    $talks[] = $talk;
    file_put_contents($talks_file, json_encode($talks));
}

function commonheader($title, $extracss = null, $extrajs = null, $extraheaders = null) {
    $actual_url = makeurl(substr($_SERVER['REQUEST_URI'], 5));

    session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="italian" />
    <meta name="robots" content="noarchive" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://www.linux.it/shared/index.php?f=bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="https://www.linux.it/shared/index.php?f=main.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo makeurl('/css/ld.css') ?>" rel="stylesheet" type="text/css" />

    <meta name="dcterms.creator" content="Italian Linux Society" />
    <link rel="publisher" href="http://www.ils.org/" />

    <meta name="twitter:title" content="<?php echo $title ?>" />
    <meta name="twitter:creator" content="@LinuxDayItalia" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:url" content="<?php echo $actual_url ?>" />
    <meta name="twitter:image" content="<?php echo makeurl('/immagini/fb.png') ?>" />

    <meta property="og:site_name" content="Linux Day" />
    <meta property="og:title" content="<?php echo $title ?>" />
    <meta property="og:url" content="<?php echo $actual_url ?>" />
    <meta property="og:image" content="<?php echo makeurl('/immagini/fb.png') ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:country-name" content="Italy" />
    <meta property="og:email" content="webmaster@linux.it" />
    <meta property="og:locale" content="it_IT" />
    <meta property="og:description" content="Giornata Nazionale per il Software Libero" />

    <script type="text/javascript" src="https://www.linux.it/shared/index.php?f=jquery.js"></script>
    <script type="text/javascript" src="https://www.linux.it/shared/index.php?f=bootstrap.js"></script>

    <?php

    if ($extraheaders != null) {
        foreach($extraheaders as $e) {
            echo $e . "\n";
        }
    }

    if ($extracss != null) {
        foreach ($extracss as $e) {
            ?>
            <link href="<?php echo $e; ?>" rel="stylesheet" type="text/css" />
            <?php
        }
    }

    if ($extrajs != null) {
        foreach ($extrajs as $e) {
            ?>
            <script type="text/javascript" src="<?php echo $e; ?>"></script>
            <?php
        }
    }

    ?>

    <title><?php echo $title; ?></title>
</head>
<body>

<div id="header">
    <img src="<?php echo makeurl('/immagini/logo.png') ?>" width="79" height="79" alt="Linux Day" />
    <div id="maintitle">Linux Day <?php echo conf('current_year') ?></div>
    <div id="payoff">Giornata Nazionale per il Software Libero</div>

    <?php

    $menu = [];

    $menu['Home'] = makeurl('/');

    if (conf('is_physical')) {
        $menu['Organizzati'] = makeurl('/howto/');
    }

    if (conf('is_virtual')) {
        if (date('Y-m-d') < conf('talks_date')) {
            $menu['Partecipa'] = makeurl('/partecipa/');
        }
        else {
            $menu['Programma'] = makeurl('/programma/');
        }
    }

    if (hasBanner()) {
        $menu['Promozione'] = makeurl('/promozione/');
    }

    if(!empty($_SESSION['user_email'])) {
        if (conf('is_physical')) {
            $menu['Il mio LinuxDay'] = makeurl('/registra/index.php');
        }
        $menu['Logout'] = makeurl('/user/?action=logout');
    }
    else {
        $menu['Login'] = makeurl('/user');
    }

    if(isset($_SESSION['admin'])) {
        $menu['Admin'] = makeurl('/admin/index.php');
    }

    ?>

    <div class="menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="main-menu">
                <ul class="navbar-nav mr-auto">
                    <?php foreach($menu as $label => $url): ?>
                    <li class="nav-item"><a class="nav-link" href="<?php echo $url ?>"><?php echo $label ?></a></li>
                    <?php endforeach ?>
                </ul>
            </div>
        </nav>

        <p class="social mt-2">
            <a href="https://twitter.com/LinuxDayItalia"><img src="https://www.linux.it/shared/index.php?f=immagini/twitter.png" alt="Linux Day su Twitter"></a>
            <a href="https://www.facebook.com/LinuxDayItalia"><img src="https://www.linux.it/shared/index.php?f=immagini/facebook.png" alt="Linux Day su Facebook"></a>
            <a href="https://gitlab.com/ItalianLinuxSociety/linuxday"><img src="https://www.linux.it/shared/?f=immagini/gitlab.svg" alt="Linux Day su GitLab"></a>
        </p>
    </div>
</div>
<?php
}

function lugheader($title, $extracss = null, $extrajs = null) {
    commonheader($title, $extracss, $extrajs);

    $ils_logo = conf('ils_logo', '/immagini/ils.png');

    ?>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-4 order-2 order-md-1 text-center promoters">
                <h5>Promosso da</h5>
                <hr>
                <a href="https://www.ils.org/">
                    <img class="img-fluid" src="<?php echo makeurl($ils_logo) ?>" alt="Italian Linux Society">
                </a>

                <?php if(!empty(conf('patronages'))): ?>
                    <hr>
                    <h5>Col Patrocinio di</h5>
                    <hr>
                    <?php foreach(conf('patronages') as $name => $meta): ?>
                        <a href="<?php echo $meta->link ?>">
                            <img class="img-fluid" src="<?php echo makeurl($meta->logo) ?>" alt="<?php echo $name ?>">
                        </a>
                    <?php endforeach ?>
                <?php endif ?>

                <?php if(!empty(conf('supporters'))): ?>
                    <hr>
                    <h5>Col Supporto di</h5>
                    <hr>
                    <?php foreach(conf('supporters') as $name => $meta): ?>
                        <a href="<?php echo $meta->link ?>">
                           <img class="img-fluid" src="<?php echo makeurl($meta->logo) ?>" alt="<?php echo $name ?>">
                        </a>
                    <?php endforeach ?>
                <?php endif ?>

                <?php if(!empty(conf('sponsors'))): ?>
                    <hr>
                    <h5>Col Sostegno di</h5>
                    <hr>
                    <?php $sponsors = conf('sponsors'); shuffle($sponsors);
                    foreach($sponsors as $name => $meta): ?>
                        <a href="<?php echo $meta->link ?>">
                            <img class="img-fluid" src="<?php echo makeurl($meta->logo) ?>" alt="<?php echo $name ?>">
                        </a>
                    <?php endforeach ?>
                <?php endif ?>
            </div>

            <div class="col-md-8 order-1 order-md-2 main-contents">

            <?php
}

function filledpageheader($title, $extracss = null, $extrajs = null, $extraheaders = null) {
    commonheader($title, $extracss, $extrajs, $extraheaders);

    ?>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12 main-contents">

    <?php
}

function fullpageheader($title, $extracss = null, $extrajs = null) {
    commonheader($title, $extracss, $extrajs);

    ?>

    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-12 main-contents">

    <?php
}

function lugfooter () {
?>

        </div>
    </div>
</div>

<div style="clear: both; margin-bottom: 20px"></div>

<div id="ils_footer" class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <span style="text-align: center; display: block">
                    <a href="http://www.gnu.org/licenses/agpl-3.0-standalone.html" rel="license">
                        <img src="https://www.linux.it/shared/index.php?f=immagini/agpl3.svg" style="border-width:0" alt="AGPLv3 License">
                    </a>

                    <a href="http://creativecommons.org/publicdomain/zero/1.0/deed.en_US" rel="license">
                        <img src="https://www.linux.it/shared/index.php?f=immagini/cczero.png" style="border-width:0" alt="Creative Commons License">
                    </a>
                </span>
            </div>

            <div class="col-md-3">
                <h2>RESTA AGGIORNATO!</h2>

                <script type="text/javascript" src="https://www.ils.org/external/widgetnewsletter.js"></script>
                <div id="widgetnewsletter"></div>
            </div>

            <div class="col-md-3">
                <h2>Amici</h2>
                <p style="text-align: center">
                    <a href="http://www.ils.org/info#aderenti">
                    <img src="https://www.ils.org/external/getrandlogo.php" border="0" /><br />
                    Scopri tutte le associazioni che hanno aderito a ILS.
                    </a>
                </p>
            </div>

            <div class="col-md-3">
                <h2>Network</h2>
                <script type="text/javaScript" src="https://www.ils.org/external/widgetils.js"></script>
                <div id="widgetils"></div>
            </div>
        </div>
    </div>

    <div style="clear: both"></div>
</div>

<!-- Matomo -->
<script type="text/javascript">
    var _paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(["setCookieDomain", "*.www.linuxday.it"]);
    _paq.push(["setDomains", ["*.www.linuxday.it","*.www.linuxday.it"]]);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u="//stats.madbob.org/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '13']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<!-- End Matomo Code -->

</body>
</html>

<?php
}

function prov_select($class, $selected = null) {
    ?>

    <select class="<?php echo $class ?>" name="prov">
        <option value="-1" <?php echo ($selected == null ? 'selected' : '') ?>>Seleziona una Provincia</option>
        <option value="AG" <?php echo ($selected == 'AG' ? 'selected' : '') ?>>Agrigento</option>
        <option value="AL" <?php echo ($selected == 'AL' ? 'selected' : '') ?>>Alessandria</option>
        <option value="AN" <?php echo ($selected == 'AN' ? 'selected' : '') ?>>Ancona</option>
        <option value="AO" <?php echo ($selected == 'AO' ? 'selected' : '') ?>>Aosta</option>
        <option value="AR" <?php echo ($selected == 'AR' ? 'selected' : '') ?>>Arezzo</option>
        <option value="AP" <?php echo ($selected == 'AP' ? 'selected' : '') ?>>Ascoli Piceno</option>
        <option value="AT" <?php echo ($selected == 'AT' ? 'selected' : '') ?>>Asti</option>
        <option value="AV" <?php echo ($selected == 'AV' ? 'selected' : '') ?>>Avellino</option>
        <option value="BA" <?php echo ($selected == 'BA' ? 'selected' : '') ?>>Bari</option>
        <option value="BT" <?php echo ($selected == 'BT' ? 'selected' : '') ?>>Barletta-Andria-Trani</option>
        <option value="BL" <?php echo ($selected == 'BL' ? 'selected' : '') ?>>Belluno</option>
        <option value="BN" <?php echo ($selected == 'BN' ? 'selected' : '') ?>>Benevento</option>
        <option value="BG" <?php echo ($selected == 'BG' ? 'selected' : '') ?>>Bergamo</option>
        <option value="BI" <?php echo ($selected == 'BI' ? 'selected' : '') ?>>Biella</option>
        <option value="BO" <?php echo ($selected == 'BO' ? 'selected' : '') ?>>Bologna</option>
        <option value="BZ" <?php echo ($selected == 'BZ' ? 'selected' : '') ?>>Bolzano</option>
        <option value="BS" <?php echo ($selected == 'BS' ? 'selected' : '') ?>>Brescia</option>
        <option value="BR" <?php echo ($selected == 'BR' ? 'selected' : '') ?>>Brindisi</option>
        <option value="CA" <?php echo ($selected == 'CA' ? 'selected' : '') ?>>Cagliari</option>
        <option value="CL" <?php echo ($selected == 'CL' ? 'selected' : '') ?>>Caltanissetta</option>
        <option value="CB" <?php echo ($selected == 'CB' ? 'selected' : '') ?>>Campobasso</option>
        <option value="CI" <?php echo ($selected == 'CI' ? 'selected' : '') ?>>Carbonia-Iglesias</option>
        <option value="CE" <?php echo ($selected == 'CE' ? 'selected' : '') ?>>Caserta</option>
        <option value="CT" <?php echo ($selected == 'CT' ? 'selected' : '') ?>>Catania</option>
        <option value="CZ" <?php echo ($selected == 'CZ' ? 'selected' : '') ?>>Catanzaro</option>
        <option value="CH" <?php echo ($selected == 'CH' ? 'selected' : '') ?>>Chieti</option>
        <option value="CO" <?php echo ($selected == 'CO' ? 'selected' : '') ?>>Como</option>
        <option value="CS" <?php echo ($selected == 'CS' ? 'selected' : '') ?>>Cosenza</option>
        <option value="CR" <?php echo ($selected == 'CR' ? 'selected' : '') ?>>Cremona</option>
        <option value="KR" <?php echo ($selected == 'KR' ? 'selected' : '') ?>>Crotone</option>
        <option value="CN" <?php echo ($selected == 'CN' ? 'selected' : '') ?>>Cuneo</option>
        <option value="EN" <?php echo ($selected == 'EN' ? 'selected' : '') ?>>Enna</option>
        <option value="FM" <?php echo ($selected == 'FM' ? 'selected' : '') ?>>Fermo</option>
        <option value="FE" <?php echo ($selected == 'FE' ? 'selected' : '') ?>>Ferrara</option>
        <option value="FI" <?php echo ($selected == 'FI' ? 'selected' : '') ?>>Firenze</option>
        <option value="FG" <?php echo ($selected == 'FG' ? 'selected' : '') ?>>Foggia</option>
        <option value="FC" <?php echo ($selected == 'FC' ? 'selected' : '') ?>>Forl&igrave;-Cesena</option>
        <option value="FR" <?php echo ($selected == 'FR' ? 'selected' : '') ?>>Frosinone</option>
        <option value="GE" <?php echo ($selected == 'GE' ? 'selected' : '') ?>>Genova</option>
        <option value="GO" <?php echo ($selected == 'GO' ? 'selected' : '') ?>>Gorizia</option>
        <option value="GR" <?php echo ($selected == 'GR' ? 'selected' : '') ?>>Grosseto</option>
        <option value="IM" <?php echo ($selected == 'IM' ? 'selected' : '') ?>>Imperia</option>
        <option value="IS" <?php echo ($selected == 'IS' ? 'selected' : '') ?>>Isernia</option>
        <option value="SP" <?php echo ($selected == 'SP' ? 'selected' : '') ?>>La Spezia</option>
        <option value="AQ" <?php echo ($selected == 'AQ' ? 'selected' : '') ?>>L'Aquila</option>
        <option value="LT" <?php echo ($selected == 'LT' ? 'selected' : '') ?>>Latina</option>
        <option value="LE" <?php echo ($selected == 'LE' ? 'selected' : '') ?>>Lecce</option>
        <option value="LC" <?php echo ($selected == 'LC' ? 'selected' : '') ?>>Lecco</option>
        <option value="LI" <?php echo ($selected == 'LI' ? 'selected' : '') ?>>Livorno</option>
        <option value="LO" <?php echo ($selected == 'LO' ? 'selected' : '') ?>>Lodi</option>
        <option value="LU" <?php echo ($selected == 'LU' ? 'selected' : '') ?>>Lucca</option>
        <option value="MC" <?php echo ($selected == 'MC' ? 'selected' : '') ?>>Macerata</option>
        <option value="MN" <?php echo ($selected == 'MN' ? 'selected' : '') ?>>Mantova</option>
        <option value="MS" <?php echo ($selected == 'MS' ? 'selected' : '') ?>>Massa-Carrara</option>
        <option value="MT" <?php echo ($selected == 'MT' ? 'selected' : '') ?>>Matera</option>
        <option value="ME" <?php echo ($selected == 'ME' ? 'selected' : '') ?>>Messina</option>
        <option value="MI" <?php echo ($selected == 'MI' ? 'selected' : '') ?>>Milano</option>
        <option value="MO" <?php echo ($selected == 'MO' ? 'selected' : '') ?>>Modena</option>
        <option value="MB" <?php echo ($selected == 'MB' ? 'selected' : '') ?>>Monza e Brianza</option>
        <option value="NA" <?php echo ($selected == 'NA' ? 'selected' : '') ?>>Napoli</option>
        <option value="NO" <?php echo ($selected == 'NO' ? 'selected' : '') ?>>Novara</option>
        <option value="NU" <?php echo ($selected == 'NU' ? 'selected' : '') ?>>Nuoro</option>
        <option value="OT" <?php echo ($selected == 'OT' ? 'selected' : '') ?>>Olbia-Tempio</option>
        <option value="OR" <?php echo ($selected == 'OR' ? 'selected' : '') ?>>Oristano</option>
        <option value="PD" <?php echo ($selected == 'PD' ? 'selected' : '') ?>>Padova</option>
        <option value="PA" <?php echo ($selected == 'PA' ? 'selected' : '') ?>>Palermo</option>
        <option value="PR" <?php echo ($selected == 'PR' ? 'selected' : '') ?>>Parma</option>
        <option value="PV" <?php echo ($selected == 'PV' ? 'selected' : '') ?>>Pavia</option>
        <option value="PG" <?php echo ($selected == 'PG' ? 'selected' : '') ?>>Perugia</option>
        <option value="PU" <?php echo ($selected == 'PU' ? 'selected' : '') ?>>Pesaro e Urbino</option>
        <option value="PE" <?php echo ($selected == 'PE' ? 'selected' : '') ?>>Pescara</option>
        <option value="PC" <?php echo ($selected == 'PC' ? 'selected' : '') ?>>Piacenza</option>
        <option value="PI" <?php echo ($selected == 'PI' ? 'selected' : '') ?>>Pisa</option>
        <option value="PT" <?php echo ($selected == 'PT' ? 'selected' : '') ?>>Pistoia</option>
        <option value="PN" <?php echo ($selected == 'PN' ? 'selected' : '') ?>>Pordenone</option>
        <option value="PZ" <?php echo ($selected == 'PZ' ? 'selected' : '') ?>>Potenza</option>
        <option value="PO" <?php echo ($selected == 'PO' ? 'selected' : '') ?>>Prato</option>
        <option value="RG" <?php echo ($selected == 'RG' ? 'selected' : '') ?>>Ragusa</option>
        <option value="RA" <?php echo ($selected == 'RA' ? 'selected' : '') ?>>Ravenna</option>
        <option value="RC" <?php echo ($selected == 'RC' ? 'selected' : '') ?>>Reggio Calabria</option>
        <option value="RE" <?php echo ($selected == 'RE' ? 'selected' : '') ?>>Reggio Emilia</option>
        <option value="RI" <?php echo ($selected == 'RI' ? 'selected' : '') ?>>Rieti</option>
        <option value="RN" <?php echo ($selected == 'RN' ? 'selected' : '') ?>>Rimini</option>
        <option value="RM" <?php echo ($selected == 'RM' ? 'selected' : '') ?>>Roma</option>
        <option value="RO" <?php echo ($selected == 'RO' ? 'selected' : '') ?>>Rovigo</option>
        <option value="SA" <?php echo ($selected == 'SA' ? 'selected' : '') ?>>Salerno</option>
        <option value="VS" <?php echo ($selected == 'VS' ? 'selected' : '') ?>>Medio Campidano</option>
        <option value="SS" <?php echo ($selected == 'SS' ? 'selected' : '') ?>>Sassari</option>
        <option value="SV" <?php echo ($selected == 'SV' ? 'selected' : '') ?>>Savona</option>
        <option value="SI" <?php echo ($selected == 'SI' ? 'selected' : '') ?>>Siena</option>
        <option value="SR" <?php echo ($selected == 'SR' ? 'selected' : '') ?>>Siracusa</option>
        <option value="SO" <?php echo ($selected == 'SO' ? 'selected' : '') ?>>Sondrio</option>
        <option value="TA" <?php echo ($selected == 'TA' ? 'selected' : '') ?>>Taranto</option>
        <option value="TE" <?php echo ($selected == 'TE' ? 'selected' : '') ?>>Teramo</option>
        <option value="TR" <?php echo ($selected == 'TR' ? 'selected' : '') ?>>Terni</option>
        <option value="TO" <?php echo ($selected == 'TO' ? 'selected' : '') ?>>Torino</option>
        <option value="OG" <?php echo ($selected == 'OG' ? 'selected' : '') ?>>Ogliastra</option>
        <option value="TP" <?php echo ($selected == 'TP' ? 'selected' : '') ?>>Trapani</option>
        <option value="TN" <?php echo ($selected == 'TN' ? 'selected' : '') ?>>Trento</option>
        <option value="TV" <?php echo ($selected == 'TV' ? 'selected' : '') ?>>Treviso</option>
        <option value="TS" <?php echo ($selected == 'TS' ? 'selected' : '') ?>>Trieste</option>
        <option value="UD" <?php echo ($selected == 'UD' ? 'selected' : '') ?>>Udine</option>
        <option value="VA" <?php echo ($selected == 'VA' ? 'selected' : '') ?>>Varese</option>
        <option value="VE" <?php echo ($selected == 'VE' ? 'selected' : '') ?>>Venezia</option>
        <option value="VB" <?php echo ($selected == 'VB' ? 'selected' : '') ?>>Verbano-Cusio-Ossola</option>
        <option value="VC" <?php echo ($selected == 'VC' ? 'selected' : '') ?>>Vercelli</option>
        <option value="VR" <?php echo ($selected == 'VR' ? 'selected' : '') ?>>Verona</option>
        <option value="VV" <?php echo ($selected == 'VV' ? 'selected' : '') ?>>Vibo Valentia</option>
        <option value="VI" <?php echo ($selected == 'VI' ? 'selected' : '') ?>>Vicenza</option>
        <option value="VT" <?php echo ($selected == 'VT' ? 'selected' : '') ?>>Viterbo</option>
    </select>

    <?php
}
